﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace IdsUpdater
{
    public class Updater
    {
        private const string BASE_URI = "https://idsroute.blob.core.windows.net/downloads",
                             VERSION_FILE_NAME = "CurrentVersion.txt",
                             DEFAULT_RESELLER = "Default",
                             UPDATES_FOLDER = @"Ids\Software Updates\";

        private const int DEFAULT_POLLING_INTERVAL_IN_MINUTES = 60;

        private readonly uint CurrentVersionNumber;

        private readonly Uri VersionUri, InstallExeUri;
        private string InstallFileName;
        private readonly string InstallerExeName;

        private Timer CheckForUpdateTimer;

        private bool Installing;

        private void CheckForUpdate( Action<Updater, uint> onUpdateAvailable )
        {
            try
            {
                byte[] Bytes;

                using( var Client = new HttpClient { Timeout = new TimeSpan( 0, 0, 0, 10 ) } )
                    Bytes = Client.GetByteArrayAsync( VersionUri ).Result;

                var Version = Encoding.UTF8.GetString( Bytes, 3, Bytes.Length - 3 ).Trim();

                if( uint.TryParse( Version, out var ServerVersion ) )
                {
                    if( ServerVersion > CurrentVersionNumber )
                    {
                        byte[] InstallBytes;
                        using( var Client = new HttpClient { Timeout = new TimeSpan( 0, 0, 15, 0 ) } )
                            InstallBytes = Client.GetByteArrayAsync( InstallExeUri ).Result;

                        var AppData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), UPDATES_FOLDER);
                        Directory.CreateDirectory( AppData );
                        InstallFileName = Path.Combine( AppData, InstallerExeName );
                        File.Delete( InstallFileName );
                        File.WriteAllBytes( InstallFileName, InstallBytes );
                        onUpdateAvailable( this, ServerVersion );
                    }
                }
            }
            catch( Exception E )
            {
                Console.WriteLine( E );
            }
        }

        private void KillTimer()
        {
            if( CheckForUpdateTimer != null )
            {
                lock( CheckForUpdateTimer )
                {
                    var T = CheckForUpdateTimer;
                    CheckForUpdateTimer = null;
                    T.Stop();
                    T.Dispose();
                }
            }
        }

        public Updater( uint currentVersionNumber, string versionTextFileName, string baseFolder, string installerExeName, Action<Updater, uint> onUpdateAvailable, string resellerAccountId = DEFAULT_RESELLER,
                        int pollingintervalInMinutes = DEFAULT_POLLING_INTERVAL_IN_MINUTES )
        {
            CurrentVersionNumber = currentVersionNumber;
            InstallerExeName = installerExeName.Trim();

            var BaseUri = $"{BASE_URI}/{baseFolder}/{resellerAccountId}/";
            VersionUri = new Uri( $"{BaseUri}{versionTextFileName}" );
            InstallExeUri = new Uri( $"{BaseUri}{installerExeName}" );

            Task.Run( () =>
            {
                CheckForUpdate( onUpdateAvailable );
                if( pollingintervalInMinutes > 0 )
                {
                    CheckForUpdateTimer = new Timer { Interval = pollingintervalInMinutes * 60 * 1000 };
                    CheckForUpdateTimer.Elapsed += ( sender, args ) =>
                    {
                        lock( CheckForUpdateTimer )
                        {
                            CheckForUpdate( ( updater, version ) =>
                            {
                                KillTimer();
                                onUpdateAvailable( updater, version );
                            } );
                        }
                    };
                    CheckForUpdateTimer.Start();
                }
            } );
        }

        public Updater( uint currentVersionNumber, string baseFolder, string installerExeName, Action<Updater, uint> onUpdateAvailable, string resellerAccountId = DEFAULT_RESELLER,
                        int pollingintervalInMinutes = DEFAULT_POLLING_INTERVAL_IN_MINUTES )
            : this( currentVersionNumber, VERSION_FILE_NAME, baseFolder, installerExeName, onUpdateAvailable, resellerAccountId, pollingintervalInMinutes )
        {
        }

        ~Updater()
        {
            KillTimer();
            if( !Installing && !string.IsNullOrEmpty( InstallFileName ) )
                File.Delete( InstallFileName );
        }

        public void RunInstall()
        {
            Installing = true;
            Process.Start( InstallFileName )?.WaitForInputIdle( 10000 );
        }
    }
}